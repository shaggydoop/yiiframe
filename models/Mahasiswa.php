<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * ContactForm is the model behind the contact form.
 */
class Mahasiswa extends ActiveRecord
{


    /**
     * @return array the validation rules.
     */
    public function rules()
    {

    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @param string $email the target email address
     * @return bool whether the model passes validation
     */
    public function view()
    {
        $views = $this::find()->all();
        return $views ;
     
    }


}
