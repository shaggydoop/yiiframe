<?php



use yii\helpers\Html;

$this->title = 'Lobbby';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-lobby">
    <h1><?= Html::encode($this->title) ?></h1>

    <!-- Modals create -->

<div class="modal fade" id="modal-insert" tabindex="-1" role="dialog"  aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" >Page Insert</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form class="form-horizontal" action="/action_page.php">
            <div class="form-group">
                <label class="control-label col-sm-2" >Nim:</label>
                <div class="col-sm-10">
                <input type="text" class="form-control" id="mim-create" placeholder="Enter Nim" >
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" >Nama:</label>
                <div class="col-sm-10">
                <input type="text" class="form-control" id="nama-create" placeholder="Enter Nama">
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-2" >Prod:</label>
                <div class="col-sm-10">
                <input type="text" class="form-control" id="prod-create" placeholder="Enter Prodi">
                </div>
            </div>
       
         
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-success clicky" >Save changes</button>
      </div>
    </div>
  </div>
</div>

    <!-- Modal Update -->
<div class="modal fade" id="modal-update" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Page Update</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form class="form-horizontal" action="/site/update">
            <div class="form-group">
                <label class="control-label col-sm-2" >Nim:</label>
                <div class="col-sm-10">
                <input type="text" class="form-control" id="mim-update" placeholder="" >
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" >Nama:</label>
                <div class="col-sm-10">
                <input type="text" class="form-control" id="nama-update" placeholder="">
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-2" >Prod:</label>
                <div class="col-sm-10">
                <input type="text" class="form-control" id="prod-update" placeholder="">
                </div>
            </div>
       
         
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-success clicky-update" >Save changes</button>
      </div>
    </div>
  </div>
</div>
<div class="w3-panel w3-card-2">
   <div class="card"> 
        <div class="container">
            <button type="button" class="btn btn-info btn-create" data-toggle="modal" data-target="#modal-insert"><i class='fa fa-plus'></i></button>           
                    <table class="table table-hover" style="">
                        <thead>
                        <tr>
                            <th>NIM</th>
                            <th>Nama</th>
                            <th>Prod</th>
                            <th width="25%" >Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($views as $row): ?>
                        <tr>
                            <td><?= ("{$row->nim}") ?></td>
                            <td><?= ("{$row->nama}") ?></td>
                            <td><?= ("{$row->prod}") ?></td>

                            <td>
                                <button class='btn btn-danger'> <i class='fa fa-trash'> </i> </button>

                                <button class='btn btn-primary btn-update' data-toggle="modal" data-target="#modal-update" > <i class='fa fa-pencil'> </i> </button>
                            </td> 
                        </tr>
                        <?php endforeach; ?>  

                    
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


<script>

$(document).ready(function(){
      $(".preloader").fadeOut();
 });


 $(document).ready(function(){

    $(".btn-update").on("click", function(){

    //before view update
    var currentRow=$(this).closest("tr"); 
    var col1=currentRow.find("td:eq(0)").text();
    var col2=currentRow.find("td:eq(1)").text();
    var col3=currentRow.find("td:eq(2)").text();
    var data=col1+"\n"+col2+"\n"+col3;
    console.log(data);
    $("#mim-update").val(col1);
    $("#nama-update").val(col2);
    $("#prod-update").val(col3);


    });

});




</script>
